package main

import (
	"os"

	"k8s.io/component-base/cli"
	_ "k8s.io/component-base/metrics/prometheus/clientgo" // for rest client metric registration
	_ "k8s.io/component-base/metrics/prometheus/version"  // for version metric registration
	"k8s.io/kubernetes/cmd/kube-scheduler/app"
	
	"sigs.k8s.io/scheduler-plugins/pkg/noderesources"

	// Ensure scheme package is initialized.
	_ "sigs.k8s.io/scheduler-plugins/apis/config/scheme"
)

func main() {
	command := app.NewSchedulerCommand(
		app.WithPlugin(noderesources.AllocatableName, noderesources.NewAllocatable),
	)

	code := cli.Run(command)
	os.Exit(code)
}