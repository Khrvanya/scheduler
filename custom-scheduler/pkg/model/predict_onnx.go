package model

import (
	"fmt"
	"path/filepath"
	"os"
	"sort"

	ort "github.com/yalue/onnxruntime_go"
)

const (
	featuresSize         = 13
	modelInputName       = "input"
	modelOutputName      = "variable"
)

// GetLastModel returns the latest ONNX model file from the specified directory
func GetLastModel(modelsPath string) (string, error) {
	modelFiles, err := filepath.Glob(filepath.Join(modelsPath, "*.onnx"))
	if err != nil {
		return "", fmt.Errorf("error reading models path: %w", err)
	}

	if len(modelFiles) == 0 {
		return "", fmt.Errorf("no models found in the path")
	}

	sort.Slice(modelFiles, func(i, j int) bool {
		fi, err := os.Stat(modelFiles[i])
		if err != nil {
			return false
		}
		fj, err := os.Stat(modelFiles[j])
		if err != nil {
			return false
		}
		return fi.ModTime().After(fj.ModTime())
	})

	return modelFiles[0], nil
}

// GetPredictions loads the latest model and makes predictions
func GetPredictions(modelsPath string, X []float32) ([]float32, error) {
	modelPath, err := GetLastModel(modelsPath)
	if err != nil {
		return nil, fmt.Errorf("error getting the latest model: %w", err)
	}
	fmt.Printf("[INFO] Model used: %s\n", modelPath)

	ort.SetSharedLibraryPath("/usr/local/lib/libonnxruntime.so")

	if err := ort.InitializeEnvironment(); err != nil {
		return nil, fmt.Errorf("failed to initialize environment: %w", err)
	}
	defer ort.DestroyEnvironment()

	inputShape := ort.NewShape(int64(len(X) / featuresSize), featuresSize)
	inputTensor, err := ort.NewTensor(inputShape, X)
	if err != nil {
		return nil, fmt.Errorf("failed to create input tensor: %w", err)
	}
	defer inputTensor.Destroy()

	outputShape := ort.NewShape(1, 1)
	outputTensor, err := ort.NewEmptyTensor[float32](outputShape)
	if err != nil {
		return nil, fmt.Errorf("failed to create output tensor: %w", err)
	}
	defer outputTensor.Destroy()

	session, err := ort.NewSession[float32](modelPath, []string{modelInputName}, []string{modelOutputName}, []*ort.Tensor[float32]{inputTensor}, []*ort.Tensor[float32]{outputTensor})
	if err != nil {
		return nil, fmt.Errorf("failed to create session: %w", err)
	}
	defer session.Destroy()

	if err := session.Run(); err != nil {
		return nil, fmt.Errorf("failed to run session: %w", err)
	}

	return outputTensor.GetData(), nil
}
