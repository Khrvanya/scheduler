package model

import (
	"fmt"
)

// PrepareFeatures prepares the features in the required format for the ONNX model input
func PrepareFeatures(data []float64) ([]float32, error) {
	n := len(data)
	if n < 360 {
		return nil, fmt.Errorf("too small DataFrame to be prepared")
	}

	lagIndices := []int{4, 14, 29, 59, 179, 359}
	maWindows := []int{5, 15, 30, 60, 180, 360}
	featureSize := 1 + len(lagIndices) + len(maWindows) // Number of lag and moving average features plus the original data

	initialDrop := max(append(lagIndices, maWindows...)...)

	// Preallocate the output slice with the required capacity
	output := make([]float32, 0, (n-initialDrop)*featureSize)

	// Precompute cumulative sum for moving averages
	cumsum := make([]float64, n)
	cumsum[0] = data[0]
	for i := 1; i < n; i++ {
		cumsum[i] = cumsum[i-1] + data[i]
	}

	row := make([]float32, featureSize)
	for i := initialDrop; i < n; i++ {

		// Reset row for new data
		row = row[:0]
		row = append(row, float32(data[i]))

		// Calculate lag features
		for _, lag := range lagIndices {
			row = append(row, float32(data[i-lag]))
		}

		// Calculate moving average features
		for _, window := range maWindows {
			sum := cumsum[i]
			if i >= window {
				sum -= cumsum[i-window]
			}
			average := sum / float64(window)
			row = append(row, float32(average))
		}

		output = append(output, row...)
	}

	return output, nil
}

func max(vals ...int) int {
	maxVal := vals[0]
	for _, val := range vals {
		if val > maxVal {
			maxVal = val
		}
	}
	return maxVal
}
