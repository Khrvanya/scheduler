package model

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

const (
	query                = `sum(rate(http_requests_total{job="custom-scheduler"}[1m]))`
	step                 = "60s"
	durationMinutes      = 360
)

// QueryResult represents the structure of the Prometheus query response
type QueryResult struct {
	Status string `json:"status"`
	Data   struct {
		ResultType string `json:"resultType"`
		Result     []struct {
			Metric struct {
				Name string `json:"__name__"`
			} `json:"metric"`
			Values [][]interface{} `json:"values"`
		} `json:"result"`
	} `json:"data"`
}

// FetchPrometheusData fetches data from Prometheus
func FetchPrometheusData(prometheusURL, query string, start, end int64, step string) ([]float64, error) {
	url := fmt.Sprintf("%s?query=%s&start=%d&end=%d&step=%s", prometheusURL, query, start, end, step)
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("error making HTTP request to Prometheus: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("error response from Prometheus: %s", resp.Status)
	}

	var result QueryResult
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return nil, fmt.Errorf("error decoding JSON response: %w", err)
	}

	if result.Status != "success" {
		return nil, fmt.Errorf("query was not successful: %s", result.Status)
	}

	dataPoints := make([]float64, 0, len(result.Data.Result)*len(result.Data.Result[0].Values))
	for _, r := range result.Data.Result {
		for _, value := range r.Values {
			val, err := strconv.ParseFloat(value[1].(string), 64)
			if err != nil {
				return nil, fmt.Errorf("error parsing value: %w", err)
			}
			dataPoints = append(dataPoints, val)
		}
	}

	return dataPoints, nil
}

func FetchData(prometheusURL string) ([]float64, error) {
	end := time.Now().Unix()
	start := end - durationMinutes*60
	prometheusURL = prometheusURL + "/api/v1/query_range"
	return FetchPrometheusData(prometheusURL, query, start, end, step)
}
