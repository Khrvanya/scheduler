/*
Copyright 2020 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package noderesources

import (
	"os"
	"k8s.io/klog/v2"
	"sigs.k8s.io/scheduler-plugins/apis/config"
)

func getEnv(key string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	klog.V(10).InfoS("Error getting env: %s", key)
	return ""
}

// resourceScorer returns a scoring function based on resource weights and mode
func resourceScorer(resToWeightMap resourceToWeightMap, mode config.ModeType) func(resourceToValueMap, resourceToValueMap) int64 {
	return func(requested, allocable resourceToValueMap) int64 {
		var nodeScore, weightSum int64
		for resource, weight := range resToWeightMap {
			resourceScore := allocable[resource]
			nodeScore += resourceScore * weight
			weightSum += weight
		}
		trafficScore := TrafficScore(getEnv("PROMETHEUS_URL"), getEnv("MODELS_PATH"))

		klog.Infof("[INFO] Node score: %d", -1 * trafficScore * (nodeScore / weightSum))

		return -1 * trafficScore * (nodeScore / weightSum)
	}
}
