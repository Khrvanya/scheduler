package noderesources

import (
	"log"
	"k8s.io/klog/v2"

	"sigs.k8s.io/scheduler-plugins/pkg/model"
)

const (
	alpha = 0.5
)

// CalculateAvgPercentageChange calculates the average percentage change
// of a predicted value considering the influence of older values.
func CalculateAvgPercentageChange(values []float64, predictedValue float64) (float64, error) {
	n := len(values)
	var sumWeights, totalWeightedPercentageInfluence float64 = 0, 0

	// Precompute the powers of alpha
	alphaPowers := make([]float64, n)
	alphaPowers[n-1] = 1
	for i := n - 2; i >= 0; i-- {
		alphaPowers[i] = alphaPowers[i+1] * alpha
	}

	// Calculate influence weights and weighted percentage influence in one loop
	for i := 0; i < n; i++ {
		if values[i] == 0 {
			continue
		}

		weight := alphaPowers[i]
		sumWeights += weight

		percentageChange := ((predictedValue - values[i]) / values[i]) * 100
		// Cap the percentage change to a maximum of 100% and a minimum of -100%
		if percentageChange > 100 {
			percentageChange = 100
		} else if percentageChange < -100 {
			percentageChange = -100
		}
		
		totalWeightedPercentageInfluence += percentageChange * weight
	}

	// Calculate the average weighted percentage influence
	averageWeightedPercentageInfluence := totalWeightedPercentageInfluence / sumWeights 
	if averageWeightedPercentageInfluence == 0 {
		averageWeightedPercentageInfluence = 1
	}

	return averageWeightedPercentageInfluence, nil
}

func TrafficScore(prometheusURL string, modelsPath string) int64 {
	data, err := model.FetchData(prometheusURL)
	if err != nil {
		log.Fatalf("Error fetching Prometheus data: %s", err)
	}

	X, err := model.PrepareFeatures(data)
	if err != nil {
		log.Fatalf("Error preparing features: %s", err)
	}
	klog.Infof("[INFO] Featured data: %v\n", X)

	prediction, err := model.GetPredictions(modelsPath, X)
	if err != nil {
		log.Fatalf("Error getting predictions: %s", err)
	}
	klog.Infof("[INFO] Prediction: %f\n", prediction[0])

	avgPercentageChange, err := CalculateAvgPercentageChange(data, float64(prediction[0]))
	if err != nil {
		log.Fatalf("Error getting avgPercentageChange: %s", err)
	}
	klog.Infof("[INFO] Average percentage change: %.2f%%", avgPercentageChange)

	return int64(avgPercentageChange)
}
