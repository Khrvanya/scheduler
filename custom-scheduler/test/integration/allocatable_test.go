package integration

import (
	"context"
	"fmt"
	"testing"
	"time"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/uuid"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/kubernetes"
	"k8s.io/kubernetes/pkg/scheduler"
	schedapi "k8s.io/kubernetes/pkg/scheduler/apis/config"
	fwkruntime "k8s.io/kubernetes/pkg/scheduler/framework/runtime"
	st "k8s.io/kubernetes/pkg/scheduler/testing"
	imageutils "k8s.io/kubernetes/test/utils/image"

	"sigs.k8s.io/scheduler-plugins/pkg/noderesources"
)

func TestAllocatablePlugin(t *testing.T) {
	testCtx := &testContext{}
	testCtx.Ctx, testCtx.CancelFn = context.WithCancel(context.Background())

	cs := kubernetes.NewForConfigOrDie(globalKubeConfig)
	testCtx.ClientSet = cs
	testCtx.KubeConfig = globalKubeConfig

	t.Log("Initializing test scheduler configuration")
	cfg, err := NewDefaultSchedulerComponentConfig()
	if err != nil {
		t.Fatalf("Failed to create scheduler component config: %v", err)
	}

	// Custom scheduler configuration setup
	t.Log("Setting up custom scheduler configuration")
	cfg.Profiles[0].Plugins.PreScore = schedapi.PluginSet{Disabled: []schedapi.Plugin{{Name: "*"}}}
	cfg.Profiles[0].Plugins.Score = schedapi.PluginSet{
		Enabled:  []schedapi.Plugin{{Name: noderesources.AllocatableName}},
		Disabled: []schedapi.Plugin{{Name: "*"}},
	}

	ns := fmt.Sprintf("integration-test-%v", string(uuid.NewUUID()))
	createNamespace(t, testCtx, ns)
	t.Logf("Created namespace %s for the test", ns)

	t.Log("Initializing the test scheduler with the allocatable plugin")
	testCtx = initTestSchedulerWithOptions(
		t,
		testCtx,
		scheduler.WithProfiles(cfg.Profiles...),
		scheduler.WithFrameworkOutOfTreeRegistry(fwkruntime.Registry{noderesources.AllocatableName: noderesources.NewAllocatable}),
	)
	t.Log("Synchronizing informer factories")
	syncInformerFactory(testCtx)
	t.Log("Synchronized informer factories")
	go testCtx.Scheduler.Run(testCtx.Ctx)
	defer cleanupTest(t, testCtx)

	// Node creation and setup for test
	t.Log("Creating test nodes")
	for _, nodeName := range []string{"fake-node-small-1", "fake-node-small-2", "fake-node-big"} {
		memory := int64(500)
		cpu := int64(2000)
		if nodeName == "fake-node-big" {
			memory = 5000
			cpu = 5000
		}


		node := st.MakeNode().Name(nodeName).Label("node", nodeName).Obj()
		node.Status.Allocatable = v1.ResourceList{
			v1.ResourcePods:   *resource.NewQuantity(32, resource.DecimalSI),
			v1.ResourceCPU:    *resource.NewMilliQuantity(cpu, resource.DecimalSI),
			v1.ResourceMemory: *resource.NewQuantity(memory, resource.DecimalSI),
		}
		node.Status.Capacity = node.Status.Allocatable

		_, err := cs.CoreV1().Nodes().Create(testCtx.Ctx, node, metav1.CreateOptions{})
		if err != nil {
			t.Fatalf("Failed to create Node %q: %v", nodeName, err)
		} else {
			t.Logf("Node %q created with cpu: %d, memory: %d", nodeName, cpu, memory)
		}
	}

	// Pod creation and scheduling assertions
	podNames := []string{"small-1", "small-2", "small-3", "small-4", "big-1"}
	pause := imageutils.GetPauseImageName()
	var pods []*v1.Pod

	for _, podName := range podNames {
		pod := st.MakePod().Namespace(ns).Name(podName).Container(pause).Obj()
		if podName == "big-1" {
			pod.Spec.Containers[0].Resources = v1.ResourceRequirements{
				Requests: v1.ResourceList{v1.ResourceCPU: *resource.NewQuantity(3, resource.DecimalSI), v1.ResourceMemory: *resource.NewQuantity(5000, resource.DecimalSI)},
			}
		} else {
			pod.Spec.Containers[0].Resources = v1.ResourceRequirements{
				Requests: v1.ResourceList{v1.ResourceCPU: *resource.NewQuantity(1, resource.DecimalSI), v1.ResourceMemory: *resource.NewQuantity(100, resource.DecimalSI)},
			}
		}

		t.Logf("Creating Pod %q", podName)
		_, err := cs.CoreV1().Pods(ns).Create(testCtx.Ctx, pod, metav1.CreateOptions{})
		if err != nil {
			t.Fatalf("Failed to create Pod %q: %v", podName, err)
		}
		pods = append(pods, pod)
	}
	defer cleanupPods(t, testCtx, pods)

	for _, pod := range pods {
		t.Logf("Checking scheduling of Pod %q", pod.Name)
		waitForPodScheduling(t, cs, pod)
		validatePodPlacement(t, cs, pod, "fake-node-big", []string{"fake-node-small-1", "fake-node-small-2"})
	}
}

// waitForPodScheduling waits for a pod to be scheduled and logs progress.
func waitForPodScheduling(t *testing.T, cs kubernetes.Interface, pod *v1.Pod) {
	t.Logf("Waiting for Pod %q to be scheduled", pod.Name)
	err := wait.Poll(1*time.Second, 60*time.Second, func() (bool, error) {
		return podScheduled(cs, pod.Namespace, pod.Name), nil
	})
	if err != nil {
		t.Fatalf("Pod %q was not scheduled within the expected time: %v", pod.Name, err)
	}
	t.Logf("Pod %q has been successfully scheduled", pod.Name)
}

// validatePodPlacement checks if the pods are placed on the expected nodes based on their designations.
func validatePodPlacement(t *testing.T, cs kubernetes.Interface, pod *v1.Pod, bigNodeName string, smallNodeNames []string) {
    retrievedPod, err := cs.CoreV1().Pods(pod.Namespace).Get(context.Background(), pod.Name, metav1.GetOptions{})
    if err != nil {
        t.Fatalf("Failed to retrieve Pod %q after scheduling: %v", pod.Name, err)
    }

    // Check if the big pod is on the big node.
    if pod.Name == "big-1" {
        if retrievedPod.Spec.NodeName != bigNodeName {
            t.Errorf("Big pod %q was expected on node %q, but was found on node %q", pod.Name, bigNodeName, retrievedPod.Spec.NodeName)
        } else {
            t.Logf("Big pod %q is correctly scheduled on the big node %q", pod.Name, bigNodeName)
        }
    } else {
        // Check if the small pods are on the small nodes.
        isSmallNode := false
        for _, smallNodeName := range smallNodeNames {
            if retrievedPod.Spec.NodeName == smallNodeName {
                isSmallNode = true
                break
            }
        }

        if !isSmallNode {
            t.Errorf("Small pod %q was not scheduled on one of the small nodes %v, but on node %q", pod.Name, smallNodeNames, retrievedPod.Spec.NodeName)
        } else {
            t.Logf("Small pod %q is correctly scheduled on one of the small nodes %v", pod.Name, smallNodeNames)
        }
    }
}

