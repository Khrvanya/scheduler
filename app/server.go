package main

import (
    "fmt"
    "log"
    "net/http"
    "golang.org/x/crypto/bcrypt"
    "os"
    "github.com/prometheus/client_golang/prometheus"
    "github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
    httpRequestsTotal = prometheus.NewCounterVec(
        prometheus.CounterOpts{
            Name: "http_requests_total",
            Help: "Total number of HTTP requests",
        },
        []string{"pod"},
    )
)

// slowHashOperation performs a CPU-intensive calculation
func slowHashOperation() {
    password := []byte("this_is_a_password")
    cost := 7 // Adjust the cost parameter to control the time (10.593235ms per request) complexity

    _, err := bcrypt.GenerateFromPassword(password, cost)
    if err != nil {
        log.Fatal(err)
    }
}

func init() {
    prometheus.MustRegister(httpRequestsTotal)
}

func requestHandler(w http.ResponseWriter, r *http.Request) {
    hostname, err := os.Hostname()
    if err != nil {
        log.Fatalf("Could not get hostname: %s\n", err)
    }
    httpRequestsTotal.WithLabelValues(hostname).Inc()

    // Perform CPU-intensive operation
    slowHashOperation()  // ~10s

    log.Printf("Handling request on pod: %s", hostname)
    fmt.Fprintf(w, "Hello, World from %s!", hostname)
}

func main() {
    http.HandleFunc("/", requestHandler)
    http.Handle("/metrics", promhttp.Handler())

    log.Println("Starting server on :8080")
    if err := http.ListenAndServe(":8080", nil); err != nil {
        log.Fatalf("Could not start server: %s\n", err)
    }
}
