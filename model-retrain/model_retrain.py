import os
import datetime
import numpy as np
from sklearn.model_selection import train_test_split

from src.dataset import get_last_dataset
from src.model import train_model, evaluate_model
from src.onnx import save_onnx_model

DATSETS_PATH = os.getenv("DATSETS_PATH")
MODELS_PATH = os.getenv("MODELS_PATH")


def main():    
    # Define features and target
    X, y = get_last_dataset(DATSETS_PATH)
    print(f"Dataset {DATSETS_PATH} is loaded. Shape {X.shape}")

    # Split data
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, shuffle=False)
    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, shuffle=False)
    
    # Train the model
    print(f"Model training started...")
    model = train_model(X_train, y_train, X_val, y_val)
    print(f"Model training finished.")

    # Evaluate the model
    predictions = evaluate_model(model, X_test, y_test)
    print(predictions)

    # Save model
    model_path = f"{MODELS_PATH}/model_{datetime.datetime.now().strftime('%Y%m%d_%H%M')}.onnx"
    save_onnx_model(model, model_path)


if __name__ == "__main__":
    main()
