pandas==1.3.5
numpy==1.21.6
scikit-learn==1.5.0
lightgbm==3.3.1
onnxruntime==1.18.0
onnxmltools==1.12.0
onnxconverter_common==1.13.0
