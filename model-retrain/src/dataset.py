import glob
import pandas as pd

def get_last_dataset(datasets_path):
    data_files = sorted(glob.glob(f"{datasets_path}/*.csv"))
    if data_files:
        data_path = data_files[-1]
        print(f"Dataset {data_path} is used!")
    else:
        raise Exception("No datasets on the path!")
    data = pd.read_csv(data_path, index_col=0)
    return data.drop(columns=["y"]), data["y"]
