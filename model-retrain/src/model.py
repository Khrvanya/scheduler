import lightgbm as lgb
from sklearn.metrics import root_mean_squared_error

def train_model(X_train, y_train, X_val, y_val):
    train_dataset = lgb.Dataset(X_train, label=y_train)
    val_dataset = lgb.Dataset(X_val, label=y_val, reference=train_dataset)

    params = {
        'objective': 'regression',
        'metric': 'rmse',
        'learning_rate': 0.01,
        'num_leaves': 31,
        'feature_fraction': 0.9,
        'bagging_fraction': 0.8,
        'bagging_freq': 5,
        'verbose': 1,
        'n_jobs': -1 
    }

    evals_result = {}
    callbacks = [lgb.early_stopping(stopping_rounds=50), lgb.record_evaluation(evals_result)]
    model = lgb.train(params, train_dataset, num_boost_round=1000, valid_sets=[train_dataset, val_dataset], callbacks=callbacks)
    return model, evals_result

def evaluate_model(model, X_val, y_val):
    predictions = model.predict(X_val)
    rmse = root_mean_squared_error(y_val, predictions)
    print(f'RMSE: {rmse}')
    return predictions
