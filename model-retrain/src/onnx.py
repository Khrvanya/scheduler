import onnxmltools
import onnxruntime as rt

def save_onnx_model(model, output_path):
    initial_type = [('input', onnxmltools.convert.common.data_types.FloatTensorType([None, model.num_feature()]))]
    onnx_model = onnxmltools.convert.convert_lightgbm(model, initial_types=initial_type)

    with open(output_path, "wb") as f:
        f.write(onnx_model.SerializeToString())
    print(f'Model saved to {output_path}')

def load_onnx_model(onnx_path):
    sess = rt.InferenceSession(onnx_path)
    return sess

def predict_with_onnx_model(sess, X):
    input_name = sess.get_inputs()[0].name    
    predictions = sess.run(None, {input_name: X.astype(np.float32)})[0]
    return predictions.flatten()

