CREATE TABLE argo-workflows-374412.traffic_dataset.google_analytics_traffic AS
SELECT
  TIMESTAMP_TRUNC(TIMESTAMP_SECONDS(visitStartTime), MINUTE) AS timestamp,
  COUNT(*) AS y
FROM
  `bigquery-public-data.google_analytics_sample.ga_sessions_*`,
  UNNEST(hits) AS hits
GROUP BY
  timestamp
ORDER BY
  timestamp