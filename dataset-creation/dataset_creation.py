import os
import pandas as pd
import numpy as np
from src.features import prepare_features
from src.prometheus import query_traffic_data

DATASETS_PATH = "src" # os.getenv("DATASETS_PATH")
PREDICTION_AHEAD = 15  # os.getenv("PREDICTION_AHEAD")
PREDICTION_BASE = 10080  # os.getenv("PREDICTION_BASE")
PROMETHEUS_URL = "http://35.188.206.105:9090"  # os.getenv("PROMETHEUS_URL")


def main():    
    parsed_data = query_traffic_data(PROMETHEUS_URL, PREDICTION_BASE)
    print(parsed_data)

    parsed_feature_data = prepare_features(parsed_data, PREDICTION_AHEAD)

    print(parsed_feature_data)

    # curr_dataset = get_last_dataset(DATASETS_PATH)
    # new_dataset = pd.concat([curr_dataset, parsed_feature_data], ignore_index=True)[parsed_feature_data.shape[0]:]

    # new_dataset_path = f"{DATASETS_PATH}/dataset_{datetime.datetime.now().strftime('%Y%m%d_%H%M')}.csv"
    # feature_data.to_csv(new_dataset_path)

    


if __name__ == "__main__":
    main()
