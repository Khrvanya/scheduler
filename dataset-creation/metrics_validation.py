import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import lightgbm as lgb
from sklearn.metrics import mean_squared_error

from src.dataset import get_X_y, generate_data
from src.utils import save_onnx_model, load_onnx_model, predict_with_onnx_model

# from src.utils import *

PROMETHEUS_URL = os.getenv("PROMETHEUS_URL")
DATASETS_PATH = os.getenv("DATASETS_PATH")


def create_new_dataset(old_dataset):
    queried_data = query_traffic_data(PROMETHEUS_URL, shift)
    new_dataset = np.concatenate((old_dataset, queried_data))[shift:]
    return new_dataset

def main():
    # Generate data
    data = generate_data(100_000)
    X, y = get_X_y(data, 2)

    print(data[10080:].head())
    print(data[10080:].shape)
    
    print(y.head())
    print(y.shape)
     
    s = load_onnx_model(model_path)
    pred = predict_with_onnx_model(s, X_test.to_numpy())
    print(np.allclose(pred, predictions, atol=1e-8))


if __name__ == "__main__":
    main()

# # Example time series data
# data = np.sin(np.linspace(0, 100, 1000))
# queried_data = query_traffic_data(PROMETHEUS_URL, 3)
# new_data = np.concatenate((data, queried_data))[3:]
# print(f"queried_data: {queried_data}") 

# # Parameters
# window_size = 10
# batch_size = 32
# input_size = 1
# hidden_size = 50
# output_size = 1

# # Split data
# train_data, test_data = train_test_split(data, test_size=0.2, shuffle=False)

# # Datasets
# train_dataset = TimeSeriesDataset(train_data, window_size)
# test_dataset = TimeSeriesDataset(test_data, window_size)

# # DataLoaders
# train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
# test_dataloader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False)

# # Trainer with early stopping
# early_stop_callback = pl.callbacks.EarlyStopping(
#     monitor='val_loss',
#     min_delta=0.00,
#     patience=5,
#     verbose=True,
#     mode='min'
# )

# model = TimeSeriesRNN(input_size, hidden_size, output_size)
# trainer = pl.Trainer(max_epochs=10, log_every_n_steps=2, callbacks=[early_stop_callback])
# trainer.fit(model, train_dataloader, val_dataloaders=test_dataloader)

# # Export the model
# model_path = f"models/model_{datetime.datetime.now().strftime('%Y%m%d_%H%M')}_init"
# export_to_onnx(model, f"{model_path}.onnx", torch.randn(1, window_size, input_size))
# export_to_pth(model, f"{model_path}.pth", input_size, hidden_size, output_size)