import time
import requests
import numpy as np
import pandas as pd

def get_oldest_timestamp(prometheus_url):
    query = "time() - prometheus_tsdb_head_max_time{job='prometheus'}"
    url = f"{prometheus_url}/api/v1/query"
    params = {
        "query": query
    }
    response = requests.get(url, params=params)
    response.raise_for_status()
    result = response.json()

    if result['status'] != 'success':
        raise Exception(f"Query was not successful: {result['status']}")

    # Calculate the oldest timestamp
    oldest_time_in_seconds = time.time() - float(result['data']['result'][0]['value'][1])
    return int(oldest_time_in_seconds)
    
def query_prometheus(prometheus_url, query, start, end, step):
    url = f"{prometheus_url}/api/v1/query_range"
    params = {
        "query": query,
        "start": start,
        "end": end,
        "step": step
    }
    response = requests.get(url, params=params)
    response.raise_for_status()
    result = response.json()

    if result['status'] != 'success':
        raise Exception(f"Query was not successful: {result['status']}")

    data_points = []
    for r in result['data']['result']:
        for value in r['values']:
            data_points.append(float(value[1]))

    return data_points
    
def query_traffic_data(prometheus_url, length):
    if prometheus_url is None:
        raise ValueError("The PROMETHEUS_URL environment variable is not set.")

    # State query
    query = "sum(rate(http_requests_total[1m]))"
    end = int(time.time())
    start = end - length*60  
    step = "60s"

    # Query Prometheus for new data
    query_data = query_prometheus(prometheus_url, query, start, end, step)
    return pd.DataFrame(query_data, columns=['y'])
