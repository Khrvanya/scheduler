import pandas as pd

def create_lag_features(df, lags, n_ahead):
    for lag in lags:
        df[f'lag_{lag}'] = df['y'].shift(lag + n_ahead - 1)
    return df

def create_moving_average_features(df, windows, n_ahead):
    for window in windows:
        df[f'ma_{window}'] = df['y'].shift(n_ahead).rolling(window=window).mean()
    return df

def prepare_features(data: pd.DataFrame, n_ahead):
    lags = [1, 5, 15, 30, 60, 180, 360]  #, 720, 1440, 10080]  # Lags in minutes
    ma_windows = [5, 15, 30, 60, 180, 360]  #, 720, 1440, 10080]  # Windows in minutes

    data = create_lag_features(data, lags, n_ahead)
    data = create_moving_average_features(data, ma_windows, n_ahead)

    initial_drop = max(max(lags), max(ma_windows)) + n_ahead - 1
    data = data.iloc[initial_drop:].dropna(axis=0)
    return data
